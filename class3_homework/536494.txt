I like that the course combines covering git basics with interesting information 
about git internal processes. The size of the group is optimal since anyone can ask
any questions and instructors are polite and technical in their answers.
I also appreciate practical demonstrations, because the examples on the slides
sometimes doesn't give complete insight into the command functionality.
However, as was mentioned in the first class, slides don't serve as
study material.

I have already tried all bacis git commands in order to create my
own project with remote origin on GitLab.
